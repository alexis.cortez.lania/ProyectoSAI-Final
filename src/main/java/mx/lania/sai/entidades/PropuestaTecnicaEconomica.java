/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.sai.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexiscortez
 */
@Entity
@Table(name = "PROPUESTA_TECNICA_ECONOMICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropuestaTecnicaEconomica.findAll", query = "SELECT p FROM PropuestaTecnicaEconomica p")
    , @NamedQuery(name = "PropuestaTecnicaEconomica.findByIdpropuesta", query = "SELECT p FROM PropuestaTecnicaEconomica p WHERE p.idpropuesta = :idpropuesta")
    , @NamedQuery(name = "PropuestaTecnicaEconomica.findByNombreLicitacion", query = "SELECT p FROM PropuestaTecnicaEconomica p WHERE p.nombreLicitacion = :nombreLicitacion")
    , @NamedQuery(name = "PropuestaTecnicaEconomica.findByArea", query = "SELECT p FROM PropuestaTecnicaEconomica p WHERE p.area = :area")
    , @NamedQuery(name = "PropuestaTecnicaEconomica.findByPresupuesto", query = "SELECT p FROM PropuestaTecnicaEconomica p WHERE p.presupuesto = :presupuesto")
    , @NamedQuery(name = "PropuestaTecnicaEconomica.findByResponsableObra", query = "SELECT p FROM PropuestaTecnicaEconomica p WHERE p.responsableObra = :responsableObra")})
public class PropuestaTecnicaEconomica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPROPUESTA")
    private Integer idpropuesta;
    @Size(max = 60)
    @Column(name = "NOMBRE_LICITACION")
    private String nombreLicitacion;
    @Size(max = 60)
    @Column(name = "AREA")
    private String area;
    @Column(name = "PRESUPUESTO")
    private Integer presupuesto;
    @Size(max = 60)
    @Column(name = "RESPONSABLE_OBRA")
    private String responsableObra;

    public PropuestaTecnicaEconomica() {
    }

    public PropuestaTecnicaEconomica(Integer idpropuesta) {
        this.idpropuesta = idpropuesta;
    }

    public Integer getIdpropuesta() {
        return idpropuesta;
    }

    public void setIdpropuesta(Integer idpropuesta) {
        this.idpropuesta = idpropuesta;
    }

    public String getNombreLicitacion() {
        return nombreLicitacion;
    }

    public void setNombreLicitacion(String nombreLicitacion) {
        this.nombreLicitacion = nombreLicitacion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(Integer presupuesto) {
        this.presupuesto = presupuesto;
    }

    public String getResponsableObra() {
        return responsableObra;
    }

    public void setResponsableObra(String responsableObra) {
        this.responsableObra = responsableObra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpropuesta != null ? idpropuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropuestaTecnicaEconomica)) {
            return false;
        }
        PropuestaTecnicaEconomica other = (PropuestaTecnicaEconomica) object;
        if ((this.idpropuesta == null && other.idpropuesta != null) || (this.idpropuesta != null && !this.idpropuesta.equals(other.idpropuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.sai.entidades.PropuestaTecnicaEconomica[ idpropuesta=" + idpropuesta + " ]";
    }
    
}
