package mx.lania.sai.oad;

import java.util.List;
import mx.lania.sai.entidades.Participantes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface OadParticipantes extends JpaRepository<Participantes, Integer> {
    
}
