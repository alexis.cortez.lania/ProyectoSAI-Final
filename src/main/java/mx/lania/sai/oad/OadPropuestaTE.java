package mx.lania.sai.oad;

import mx.lania.sai.entidades.PropuestaTecnicaEconomica;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author PCEL
 */
public interface OadPropuestaTE extends JpaRepository<PropuestaTecnicaEconomica, Integer>{
    PropuestaTecnicaEconomica findByNombre(String nombre);
    
}
