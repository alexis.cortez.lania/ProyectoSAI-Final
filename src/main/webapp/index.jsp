<%-- 
    Document   : index
    Created on : 22-ago-2018, 11:43:24
    Author     : alexiscortez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appSAI">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.js"></script>
        <script src="js/ctrParticipantes.js"></script>
        <link rel=StyleSheet href="css/estilos.css" type="text/css">
        <title>Participantes</title>
    </head>
    <body>
        <div class="contenedor">
            <div class="titulo"><h1>Participantes</h1></div>
            <div class="btnagregar"><button><a href="agregarParticipante.jsp">NUEVO Participante</a></button></div>
            <br>
        <div ng-controller="ctrlParticipantes">
            <table cellpading="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre Empresa</th>
                        <th>Direccion</th>
                        <th>Representante</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="l in listaLibros">
                        <td>{{l.id}}</td>
                        <td>{{l.titulo}}</td>
                        <td>{{l.autor}}</td>
                        <td>{{l.autor}}</td>
                        <td>{{l.fechaPublicacion | date : 'dd/MM/yyyy'}}</td>
                        <td>{{l.numeroPag}}</td>
                        <td class="editar"><button class="btnagregar"><a href="" ng-click="borrarLibro(l.id)">Eliminar</a></button></td>
                    </tr>
                </tbody>
            </table><br/>
        </div>
        </div>
    </body>
</html>
