var appSAI = angular.module('appSAI', []);

appSAI.controller('ctrlParticipantes', ['$scope', '$http', '$log',
    function ($scope, $http, $log) {
        $log.debug('Definiendo controlador....');
        $scope.listaParticipantes = [];
        $scope.idparticipante = 0;
        $scope.nuevoPart = {};
        
        //Mostrar Participantes
        $scope.cargarParticipantes = function () {
            $http.get('participantes')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        //NO ES UTIL: $log.debug("Datos: "+ respuesta.data);
                        $scope.listaParticipantes = respuesta.data;
                    }, function (respuesta) {
                        $log.debug("Participantes cargardos");
                    });
        };
        
        //Agregar libro
        $scope.guardarParticipante = function () {
            $log.debug("guardarParticipante");
            $http.post('participante', $scope.nuevoPart)
                    .then(function(){
                       $log.debug("POST EXITO");
                       $scope.nuevoPart = {};
                       $scope.cargarParticipantes();
                       location.href = "index.jsp";
                    }, function(respuesta){
                        $log.debug("POST ERROR");
                    });
        };
        
        //Borrar libro
        $scope.borrarParticipante = function (id) {
            $log.debug("Borrado exito, id:" + id);
            $http.delete('participante/'+id,)
                    .then(function(){
                       $log.debug("Borrado exito");
                       $scope.cargarParticipantes();
                    }, function(respuesta){
                        $log.debug("Borrado error");
                    });
        };
        $scope.cargarParticipantes;
        $log.debug('Control definido');

    }]);