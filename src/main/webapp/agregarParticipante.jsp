<%-- 
    Document   : agregarParticipante
    Created on : 22-ago-2018, 13:13:11
    Author     : alexiscortez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="appSAI">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.js"></script>
        <script src="js/ctrLibros.js"></script>
        <link rel=StyleSheet href="css/formularios.css" type="text/css">
        <title>Agregar Libro</title>
    </head>
    <body>
        <div class="contenedor" ng-controller="ctrlParticipantes">
            <div class="titulo"><h1>Agregar Nuevo Participante</h1></div>
            <div id="forma"> 
            <table cellpadding="0" cellspacing="0">
            <tr>
                <td>Nombre de la empresa</td>
                <td><input type="text" value="" ng-model="nuevoPart.nombreEmpresa"></td>
            </tr>
            <tr>
                <td>Direccion</td>
                <td><input type="text" value="" ng-model="nuevoPart.direccion"></td>
            </tr>
            <tr>
                <td>Representante</td>
                <td><input type="text" value="" ng-model="nuevoPart.representante"></td>
            </tr>
            <tr class="boton">
                <td><input type="button" class="btnagregar" value="Agregar" ng-click="guardarParticipante()"></td>
                <td><input type="button" class="btnagregar" value="Regresar" onclick="window.location = 'index.jsp'"></td>
            </tr>
        </table>
            </div>
        </div>
    </body>
</html>
